# Change your DNS config

    vim /etc/hosts

# Add this line into the file
### It'll redirect the domain to your localhost
    127.0.0.1       criteotilt.com


# Issue this command to run server

    sudo python3 -m http.server 80

# Open [criteotilt.com](http://www.criteotilt.com.com) in Chrome