
const { chromium, webkit, firefox } = require('playwright');
var _ = require('lodash');

(async () => {
  const browser = await chromium.launch({
            headless: false,
        });
  const page = await browser.newPage();
  await page.setViewportSize({
    width: 1920,
    height: 1080,
  });

  // Subscribe to 'request' and 'response' events.

  page.on('request', request => {{
    // Checinkg CDB requests
    if (request.url().includes('cdb?')) {
        console.log('>>', request.url())   
        }
    // Checinkg Prebid requests
    if (request.url().includes('prebid')) {
        console.log('>>', request.url())   
        }
    // Checinkg google requests
    if (request.url().includes('ads?gdfp')) {
        console.log('>>', request.url())   
        }
    // Checinkg gum.criteo.com requests
    if (request.url().includes('gum.criteo.com')) {
        console.log('>>', request.url())   
        }
}});
      
  page.on('response', response => {{
    // Checinkg CDB response
    if (response.url().includes('cdb?')) {
    console.log('<<', response.status(), response.url())   
    }    
    // Checinkg Prebid response
    if (response.url().includes('prebid')) {
        console.log('<<', response.status(), response.url())  
        }
    // Checinkg Google response
    if (response.url().includes('ads?gdfp')) {
        console.log('<<', response.status(), response.url())  
        }
    
    // Checinkg if Website is blocked for iframe
    if (response.headers()) {
        key = Object.keys(response.headers())
        value = Object.values(response.headers())
        for (var i = 0; i < _.keys(key).length; i++) {
            if (key[i] == 'x-frame-options'){
                console.log(response.url(), "Doesn't allow <Iframe>")
                console.log("Check Header Response", key[i], value[i])
                // ****** Falta agregar un request fake para probar ******
            }
         }
        }
    }});

  await page.goto('http://localhost:3000/', {waitUntil: 'load', timeout: 0});

  //await browser.close();
})();